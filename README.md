
Link to Video: https://drive.google.com/file/d/1nzT-kpXkJ73m-i8eqGnLhZHx824c8OHC/view?usp=sharing

Link to Slides: https://docs.google.com/presentation/d/1GTw6hTeSnZ8oczLmib0nfJVRDz_MGkhu_p4dW_vGcYM/edit?usp=sharing

**Description**

ENGCalc will be an interactive engineering calculator. It will have many important engineering equations spanning different engineering subfields. It will allow users to select an equation, input values for the known variables, and automatically solve for the unknowns. It will support both numerical calculation and symbolic calculation, and a combination of both if more than one variable is unknown. 

Additional functionality will include the ability for a user to add equations for themselves, favorite certain equations that they use frequently, and to copy and paste equation results easily. Furthermore, if time allows, we may integrate a chatbot API that will allow users to ask questions about the equation they have selected, informing them of its uses and applications.


**API Technologies**

math.js API: This is an open-source library that will allow our application to perform calculations upon larger, more complex numbers, given input of a certain format. The appeal of this API, as opposed to others, is that it involves HTTPS requests, which is a feature that prior assignments have utilized.

SymPy API: This is a math API that our application may utilize. It is a Python library that is able to work with series, matrices, integrals, and other forms of calculation that would be especially helpful for an engineering calculator.

Overleaf API: This is a latex formatting API that will allow us to nicely display our equations and variables, adding to the UI and the overall complexity.


**Sources of Complexity**

One source of complexity is integrating an intelligent calculation engine (such as math.js or SymPy) that is capable of handling a combination of numerical and symbolic calculation, and can handle complex calculations such as indefinite integrals and partial differential equations. Additionally, there will be the ability for users to change the units of each variable, and our app will automatically update the numerical value. 

Allowing the user to add their own equations may add more complexity that we do not yet fully understand. It will certainly require the use of SwiftData, and depending on the APIs we use it may create even more complexity.
