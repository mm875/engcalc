//
//  MathLoader.swift
//  ENGCalc
//
//  Created by Michael on 3/27/24.
//

import Foundation
import SwiftUI

@Observable
class MathLoader {
    let apiClient = MathAPIClient()
    private(set) var state: LoadingState = .idle
    
    enum LoadingState {
        case idle
        case loading
        case success(data: String)
        case failed(error: Error)
    }
    
    @MainActor
    func loadResult(input: Equation) async {
        self.state = .loading
        do {
            let result = try await apiClient.solveForVariable(eq: input)
            self.state = .success(data: result)
        } catch {
            self.state = .failed(error: error)
        }
    }
}
