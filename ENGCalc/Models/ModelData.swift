//
//  ModelData.swift
//  ENGCalc
//
//  Created by Michael on 4/11/24.
//

import Foundation
import SwiftData
import UIKit

class ModelData {
    
    static func startingData(modelContext: ModelContext) {
        let all = Folder.previewData[0]
        let defaults = Folder.previewData[1]
        Equation.previewData.forEach {
            $0.folderList.append(all.name)
            $0.folderList.append(defaults.name)
            modelContext.insert($0)
        }
        Folder.previewData.forEach { modelContext.insert($0) }   
    }
    
    @MainActor
    static var previewContainer: ModelContainer {
        let schema = Schema([Equation.self, Folder.self])
        let modelConfiguration = ModelConfiguration(schema: schema, isStoredInMemoryOnly: true)
        let container = try! ModelContainer(for: schema, configurations: modelConfiguration)
        ModelData.startingData(modelContext: container.mainContext)
        return container
    }
    
}
