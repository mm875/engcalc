
import SwiftUI
import SwiftData


@Model
class Equation: Identifiable {
    var name        : String
    var details     : String
    var plainEquation : String
    var latex       : String
    var notes       : String
    var fav         : Bool {folderList.contains(where: {$0.lowercased() == "favorites"})}
    var deletable   : Bool
    var lastSolveVar: Variable?
    var thumbnailUrl: URL?
    var folder  : [Folder] = []
    
    var folderList  : [String] = []
    
    var variables: [Variable] = []
    
    
    init(name: String = "", variables: [Variable] = [], details: String = "", plainEquation: String = "", latex: String = "", notes: String = "", deletable: Bool = false, lastSolveVar: Variable? = nil, folderList: [String] = [], thumbnailUrl: URL? = nil) {
        self.name = name
        self.variables = variables
        self.details = details
        self.plainEquation = plainEquation.lowercased()
        self.latex = latex
        self.notes = notes
        self.deletable = deletable
        self.lastSolveVar = lastSolveVar ?? nil
        self.folderList = folderList
        self.thumbnailUrl = thumbnailUrl
    }
    
    func changeValues(name: String, variables: [Variable], details: String, plainEquation: String, latex: String, notes: String, deletable: Bool, lastSolveVar: Variable?, folderList: [String] , thumbnailUrl: URL?) -> Equation {
        
        return Equation(
            name: name ,
            variables: variables ,
            details: details ,
            plainEquation: plainEquation ,
            latex: latex ,
            notes: notes ,
            deletable: deletable ,
            lastSolveVar: lastSolveVar ?? self.lastSolveVar,
            folderList: folderList,
            thumbnailUrl: thumbnailUrl
        )
    }
}

extension Equation {
    
    struct FormData: Identifiable {
        var id: UUID = UUID()
        var name        : String = ""
        var variables   : [Variable] = []
        var details     : String = ""
        var plainEquation : String = ""
        var deletable  : Bool = true
        var latex       : String = ""
        var notes       : String = ""
        var folderList  : [String] = []
        var thumbnailUrl: String = ""
    }
    
    var dataForForm: FormData {
        FormData(
            name: name,
            variables: variables,
            details: details,
            plainEquation: plainEquation,
            deletable: deletable,
            latex: latex,
            notes: notes,
            folderList: folderList,
            thumbnailUrl: thumbnailUrl?.absoluteString ?? ""
        )
    }
    
    static func create(from formData: FormData, context: ModelContext) {
        
        let equation = Equation(
            name: formData.name,
            variables: formData.variables,
            details: formData.details,
            plainEquation: formData.plainEquation,
            latex: formData.latex,
            notes: formData.notes,
            deletable: true,          // User made equations are all deletable
            lastSolveVar: nil,
            folderList: formData.folderList,
            thumbnailUrl: URL(string: formData.thumbnailUrl)
        )
        Equation.update(equation, from: formData)
        equation.folderList.append(contentsOf: ["all", "user made"])
        context.insert(equation)
    }
    
    static func update(_ equation: Equation, from formData: FormData) {
        equation.name = formData.name
        equation.variables = formData.variables
        equation.details = formData.details
        equation.plainEquation = formData.plainEquation.lowercased()
        equation.latex = formData.latex
        equation.notes = formData.notes
        equation.deletable = formData.deletable
        equation.lastSolveVar = nil
        equation.folderList = formData.folderList
        equation.thumbnailUrl = URL(string: formData.thumbnailUrl)
    }
    
}


struct Variable : Identifiable, Codable, Hashable{
    var id:UUID     = UUID()
    var name        : String
    var symbol      : String
    var lowerCaseSym: String {symbol.lowercased()}
    var value       : Float? = nil
    var valueAsString: String = ""
    var unit        : String
}

extension Equation {
    static let previewData: [Equation] = [
        Equation(
            name: "Newton's Second Law",
            variables: [
                Variable(name: "Force", symbol: "F", value: nil, unit: "N"),
                Variable(name: "Mass", symbol: "m", value: nil, unit: "kg"),
                Variable(name: "Acceleration", symbol: "a", value: nil, unit: "m/s^2")
            ],
            
            details: "Newton's second law of motion states that the acceleration of an object is directly proportional to the net force acting on it and inversely proportional to its mass.",
            plainEquation: "F=m*a",
            latex: "$F = ma$",
            notes: "This equation is fundamental in classical mechanics.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Mechanics"],
            thumbnailUrl: URL(string: "https://praxilabs.com/en/blog/wp-content/uploads/2021/11/istockphoto-1126672741-612x612-1.jpg")
        ),
        Equation(
            name: "Deflection of a Cantilever Beam",
            variables: [
                Variable(name: "Deflection", symbol: "δ", value: nil, unit: "m"),
                Variable(name: "Load", symbol: "P", value: nil, unit: "N"),
                Variable(name: "Length of the Beam", symbol: "L", value: nil, unit: "m"),
                Variable(name: "Modulus of Elasticity", symbol: "E", value: nil, unit: "Pa"),
                Variable(name: "Moment of Inertia", symbol: "I", value: nil, unit: "m^4")
            ],
            details: "The deflection at the free end of a cantilever beam subjected to a point load at its end is given by the formula below. The deflection depends on the load, length of the beam, modulus of elasticity, and moment of inertia.",
            plainEquation: "δ=(P*L^3)/(3*E*I)",
            latex: "$\\delta = \\frac{P L^3}{3EI}$",
            notes: "This equation is critical in civil and structural engineering for designing beams that can safely withstand specified loads without excessive bending or breaking.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Mechanics"],
            thumbnailUrl: URL(string: "https://www.hardwareinterviews.fyi/uploads/default/original/1X/e0c3c082db7e78822a3566fda52e62564dde2377.png")          ),
        Equation(
            name: "Deflection of a Simply Supported Beam",
            variables: [
                Variable(name: "Maximum Deflection", symbol: "δ", value: nil, unit: "m"),
                Variable(name: "Load", symbol: "P", value: nil, unit: "N"),
                Variable(name: "Length of the Beam", symbol: "L", value: nil, unit: "m"),
                Variable(name: "Modulus of Elasticity", symbol: "E", value: nil, unit: "Pa"),
                Variable(name: "Moment of Inertia", symbol: "I", value: nil, unit: "m^4")
            ],
            details: "The maximum deflection of a simply supported beam subjected to a central point load is given by this formula. The deflection is affected by the load, the length of the beam, the modulus of elasticity, and the moment of inertia of the beam's cross-section.",
            plainEquation: "δ = (P * L^3) / (48 * E * I)",
            latex: "$\\delta_{\\text{max}} = \\frac{P L^3}{48EI}$",
            notes: "This equation helps in designing beams that can withstand certain loads without excessive bending. It is crucial for ensuring safety and structural integrity in buildings and bridges.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Mechanics"],
            thumbnailUrl: URL(string: "https://uploads-cdn.omnicalculator.com/images/deflected/DEF1mod.png")
        ),
        Equation(
            name: "Reynolds Number",
            variables: [
                Variable(name: "Reynolds Number", symbol: "Re", value: nil, unit: ""),
                Variable(name: "Density", symbol: "ρ", value: nil, unit: "kg/m^3"),
                Variable(name: "Velocity", symbol: "v", value: nil, unit: "m/s"),
                Variable(name: "Characteristic Length", symbol: "L", value: nil, unit: "m"),
                Variable(name: "Dynamic Viscosity", symbol: "μ", value: nil, unit: "Pa·s")
            ],
            details: "The Reynolds Number is used to predict the transition from laminar to turbulent flow, and is defined as the ratio of inertial forces to viscous forces.",
            plainEquation: "Re=(ρ*v*L)/μ",
            latex: "$Re = \\frac{\\rho v L}{\\mu}$",
            notes: "The value of the Reynolds Number helps predict the type of flow, either laminar or turbulent. It is dimensionless and critical in the analysis of fluid flow in various applications like aircraft and naval design.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Fluids & Heat"],
            thumbnailUrl: URL(string: "https://www.bronkhorst.com/getmedia/b29e66cb-8121-47d7-ac22-a715eb24e1b8/LaminarVSTurbulent.png?width=")
        ),
        
        Equation(
            name: "Hooke's Law",
            variables: [
                Variable(name: "Force", symbol: "F", value: nil, unit: "N"),
                Variable(name: "Spring Constant", symbol: "k", value: nil, unit: "N/m"),
                Variable(name: "Displacement", symbol: "x", value: nil, unit: "m")
            ],
            details: "Hooke's Law states that the force exerted by a spring is directly proportional to the displacement caused by the load.",
            plainEquation: "F=k*x",
            latex: "$F = kx$",
            notes: "This principle is essential in mechanical engineering, covering areas such as suspension systems and stress analysis.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Electrical"],
            thumbnailUrl: URL(string: "https://cdn.britannica.com/82/146782-050-AF0DE946/kx-law-Hooke-F-force-length-displacement.jpg")
        ),
        
        Equation(
            name: "Steady State Conduction",
            variables: [
                Variable(name: "Heat Transfer Rate", symbol: "Q", value: nil, unit: "W"),
                Variable(name: "Thermal Conductivity", symbol: "k", value: nil, unit: "W/m·K"),
                Variable(name: "Temperature", symbol: "T", value: nil, unit: "K"),
                Variable(name: "Distance", symbol: "x", value: nil, unit: "m"),

                Variable(name: "Area", symbol: "A", value: nil, unit: "m^2")
            ],
            details: "Steady state conduction refers to the condition where the temperature field within a conducting medium does not change over time. The heat transfer rate in this scenario is constant and is described by Fourier's law of heat conduction.",
            plainEquation: "Q = -k * A * (T/x)",
            latex: "$Q = -k A \\frac{dT}{dx}$",
            notes: "This equation is essential for the design of thermal systems, such as heat sinks, insulators, and any application involving thermal steady states.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Fluids & Heat"],
            thumbnailUrl: URL(string: "https://media.geeksforgeeks.org/wp-content/uploads/20220324111514/hf.png")
        ),
        Equation(
            name: "Steady State Convection",
            variables: [
                Variable(name: "Convective Heat Transfer Rate", symbol: "Q", value: nil, unit: "W"),
                Variable(name: "Heat Transfer Coefficient", symbol: "h", value: nil, unit: "W/m^2·K"),
                Variable(name: "Surface Area", symbol: "A", value: nil, unit: "m^2"),
                Variable(name: "Temperature Difference", symbol: "T", value: nil, unit: "K")
            ],
            details: "Steady state convection occurs when the rate of heat transfer between a surface and a fluid flowing over it remains constant over time. The rate of convective heat transfer is described by Newton's law of cooling.",
            plainEquation: "Q = h * A * T",
            latex: "$Q = h A \\Delta T$",
            notes: "Understanding this equation is crucial for designing systems involving fluids, such as radiators, heat exchangers, and HVAC systems.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Fluids & Heat"],
            thumbnailUrl: URL(string: "https://www.engineeringtoolbox.com/docs/documents/428/conductive_heat_transfer.png")
        ),
        Equation(
            name: "Area Moment of Inertia of a Rectangle",
            variables: [
                Variable(name: "Moment of Inertia", symbol: "I", value: nil, unit: "m^4"),
                Variable(name: "Width", symbol: "b", value: nil, unit: "m"),
                Variable(name: "Height", symbol: "h", value: nil, unit: "m")
            ],
            details: "The area moment of inertia of a rectangle about an axis through its centroid, perpendicular to the height (h), is given by this formula. It is crucial for determining how a beam's cross-section can resist bending under a load.",
            plainEquation: "I = (b * h^3) / 12",
            latex: "$I = \\frac{b h^3}{12}$",
            notes: "This equation is used to calculate the bending resistance of beams, critical for structural analysis and design in various engineering applications. It helps ensure beams are designed to withstand expected loads without failing due to bending.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Mechanics"],
            thumbnailUrl: URL(string: "https://cdn.calcresource.com/images/drawing-moment-inertia-rect_xy.rev.ff7cf3324c.png")
        ),
        Equation(
            name: "Ohm's Law",
            variables: [
                Variable(name: "Voltage", symbol: "V", value: nil, unit: "V"),
                Variable(name: "Current", symbol: "I", value: nil, unit: "A"),
                Variable(name: "Resistance", symbol: "R", value: nil, unit: "Ω")
            ],
            details: "Ohm's Law states that the current flowing through a conductor between two points is directly proportional to the voltage across the two points and inversely proportional to the resistance between them.",
            plainEquation: "V = I * R",
            latex: "$V = IR$",
            notes: "This fundamental electrical equation is crucial for circuit analysis and design, allowing for calculations of voltage, current, and resistance in electrical circuits.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Electrical"],
            thumbnailUrl: URL(string: "https://media.cheggcdn.com/media/afb/afb6e596-b7d2-40da-8bfc-16c03ff83254/phpO4J6Ks")
        ),
        Equation(
            name: "Electrical Power",
            variables: [
                Variable(name: "Power", symbol: "P", value: nil, unit: "W"),
                Variable(name: "Voltage", symbol: "V", value: nil, unit: "V"),
                Variable(name: "Current", symbol: "I", value: nil, unit: "A")
            ],
            details: "The power formula in electrical engineering relates the power consumed by an electrical device to the voltage across it and the current flowing through it.",
            plainEquation: "P = V * I",
            latex: "$P = VI$",
            notes: "This equation is essential for determining the energy usage and efficiency of electrical devices. It's used extensively in the design and testing of electrical systems.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Electrical"],
            thumbnailUrl: URL(string: "https://media-cldnry.s-nbcnews.com/image/upload/t_fit-1240w,f_auto,q_auto:best/MSNBC/Components/Photo/_new/081209-light-bulb-03.jpg")
        ),
        Equation(
            name: "Capacitance of a Parallel Plate Capacitor",
            variables: [
                Variable(name: "Capacitance", symbol: "C", value: nil, unit: "F"),
                Variable(name: "Permittivity", symbol: "ε", value: nil, unit: "F/m"),
                Variable(name: "Area", symbol: "A", value: nil, unit: "m^2"),
                Variable(name: "Distance Between Plates", symbol: "d", value: nil, unit: "m")
            ],
            details: "The capacitance of a parallel plate capacitor can be calculated using the area of the plates, the distance between them, and the permittivity of the material between the plates.",
            plainEquation: "C = (ε * A) / d",
            latex: "$C = \\frac{\\epsilon A}{d}$",
            notes: "Capacitance is a key concept in the design of circuits involving AC signal processing and energy storage systems. This equation helps predict how much charge a capacitor can store.",
            deletable: false,
            lastSolveVar: nil,
            folderList: ["Electrical"],
            thumbnailUrl: URL(string: "https://i.ytimg.com/vi/3EPlhwIOXKI/maxresdefault.jpg")
        )
    ]
}


