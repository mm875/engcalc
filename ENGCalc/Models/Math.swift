//
//  Math.swift
//  ENGCalc
//
//  Created by Michael on 3/27/24.
//

import Foundation

struct NewtonResponse: Decodable {
    var operation: String
    var expression: String
    var result: String
}
