import SwiftData

@Model
class Folder: Identifiable{
    var name: String
    var deletable : Bool = true
    
    init(name: String, deletable: Bool) {
        self.name = name
        self.deletable = deletable
    }
    
    static func create(name: String, modelContext: ModelContext){
        let folderList: [Folder]? = try? modelContext.fetch(FetchDescriptor<Folder>())
        if (folderList != nil){
            if (folderList!.contains(where: {$0.name.lowercased() == name.lowercased()})){
                return
            }
        }
        let folder = Folder(name: name, deletable: true)
        modelContext.insert(folder)
    }
}

extension Folder{
    static let previewData = [
        Folder(name: "All", deletable: false),
        Folder(name: "Defaults", deletable: false),
        Folder(name: "User Made", deletable: false),
        Folder(name: "Favorites", deletable: false),
        Folder(name: "Mechanics", deletable: true),
        Folder(name: "Fluids & Heat", deletable: true),
        Folder(name: "Electrical", deletable: true)
    ]
}
