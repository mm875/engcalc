import SwiftUI

struct TabContainer: View {
    
    var body: some View {
        EquationsList()
    }
}

#Preview {
    return TabContainer().modelContainer(ModelData.previewContainer)
}
