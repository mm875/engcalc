
import SwiftUI
import SwiftData
import LaTeXSwiftUI

struct EquationDetail: View {
    
    @Environment(\.modelContext) private var modelContext
    @Bindable var eq: Equation
    @State private var isPresentingEquationForm: Bool = false
    @State private var editEquationFormData: Equation.FormData = Equation.FormData()
    @State private var varToSolve: Variable = Variable(name: "J", symbol: "lbs", value: nil, valueAsString: "", unit: "in")
    
    var body: some View {
        ScrollView{
            VStack {
                VStack {
                    Spacer()
                    Text(eq.name).font(.title2).bold()
                    Spacer()
                    Spacer()
                    LaTeX(eq.latex != "" ? eq.latex :  "$\(eq.plainEquation)$").padding(0).scaledToFit()
                    InputValues(eq: eq, varToSolve: varToSolve)
                }
                .background(RoundedRectangle(cornerRadius: 10).fill(Color.white))
                .onAppear {
                    if let firstVariable = eq.variables.first {
                        varToSolve = firstVariable
                    }
                }.padding().overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .strokeBorder(Color.accentColor, lineWidth: 3)
                )
                VStack() {
                    
                    Text("Details")
                        .font(.title3)
                        .bold()
                        .frame(maxWidth: .infinity)
                    AsyncImage(url: eq.thumbnailUrl) { image in
                        image
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 300, height: 300)
                            .cornerRadius(6)
                    } placeholder: {
                        if eq.thumbnailUrl != nil {
                            ProgressView()
                        } else {
                            Image(systemName: "function")
                                .frame(width: 50, height: 50)
                                .background(Color.red)
                                .cornerRadius(8)
                                .padding(.trailing)
                            
                        }
                    }
                    Text(eq.details).padding()
                }.padding().overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .strokeBorder(Color.accentColor, lineWidth: 3)
                )
                VStack() {
                    Text("Notes").font(.title3).bold()
                    TextEditor(text: $eq.notes)
                        .frame(height: 200)
                        .padding()
                }.padding()
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .strokeBorder(Color.accentColor, lineWidth: 3)
                    )
                Spacer()
            }.padding(.horizontal)
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button("Edit") {
                            editEquationFormData = eq.dataForForm
                            isPresentingEquationForm.toggle()
                        }
                    }
                }
                .sheet(isPresented: $isPresentingEquationForm) {
                    NavigationStack {
                        EquationForm(formData: $editEquationFormData)
                            .toolbar {
                                ToolbarItem(placement: .navigationBarLeading) {
                                    Button("Cancel") { isPresentingEquationForm.toggle() }
                                    
                                }
                                ToolbarItem(placement: .navigationBarTrailing) {
                                    Button("Save") {
                                        Equation.update(eq, from: editEquationFormData)
                                        isPresentingEquationForm.toggle()
                                    }
                                }
                            }
                    }
                }
        }
    }
}

struct InputValues: View {
    
    let eq: Equation
    let varToSolve: Variable
    @State var MathLoader = ENGCalc.MathLoader()
    @State private var isReadyToSolve = false
    
    var body: some View {
        
        let columns = [GridItem(.flexible())]
        
        LazyVGrid(columns: columns, spacing: 25) {
            ForEach(eq.variables.indices, id: \.self) { index in
                if (eq.variables[index].name != varToSolve.name){
                    HStack {
                        
                        Button {
                            if eq.variables[index].valueAsString.contains("-") {
                                eq.variables[index].valueAsString.remove(at: eq.variables[index].valueAsString.firstIndex(of: "-")!)
                            } else {
                                eq.variables[index].valueAsString.insert("-", at: eq.variables[index].valueAsString.startIndex)
                            }
                        } label: {
                            Image(systemName: "plusminus")
                                .padding(3.5)
                                .background(Color.accentColor)
                                .foregroundColor(Color.white)
                                .cornerRadius(/*@START_MENU_TOKEN@*/3.0/*@END_MENU_TOKEN@*/)
                        }.padding(.trailing, 15)
                        
                        LaTeX("$\(eq.variables[index].symbol) = $")
                            .font(.title2)
                            .frame(maxWidth: 60)
                        
                        TextField("Enter Value", text: Binding(
                            get: { eq.variables[index].valueAsString },
                            set: { newValue in
                                eq.variables[index].valueAsString = newValue
                                solvableCheck()
                            }
                        ))
                        .textFieldStyle(.roundedBorder)
                        .frame(maxWidth: 125)
                        .keyboardType(.decimalPad)
                        .toolbar {
                            ToolbarItemGroup(placement: .keyboard) {
                                Spacer()
                                
                            }
                        }
                        Spacer()
                        LaTeX("$[\(eq.variables[index].unit)]$")
                        Spacer()
                        
                        
                    }
                    
                }
            }
            
        }
        .padding(.top)
        .onAppear(perform: solvableCheck)
        
        Button {
            let newVariables: [Variable] = eq.variables.map { variable in
                var newVariable: Variable = variable
                newVariable.value = Float(variable.valueAsString)
                return newVariable
            }
            let state = Equation()
            let eqCopy = state.changeValues(name: eq.name , variables: newVariables , details: eq.details , plainEquation: eq.plainEquation , latex: eq.latex , notes: eq.notes ,  deletable: eq.deletable , lastSolveVar: eq.lastSolveVar, folderList: eq.folderList, thumbnailUrl: eq.thumbnailUrl)
            Task {
                await MathLoader.loadResult(input: eqCopy)
                isReadyToSolve = false
            }
        }
    label:{
        LaTeX("$Solve$")
    }
    .buttonStyle(.borderedProminent)
    .disabled(!isReadyToSolve)
        
        switch MathLoader.state {
        case .idle: Color.clear
        case .loading: ProgressView()
        case .failed(let error): ScrollView { Text("Error \(error.localizedDescription)") }
        case .success(let result):
            LaTeX("$\(varToSolve.symbol) = \(result) ~ \(varToSolve.unit)$")
                .padding()
                .font(.title)
                .foregroundColor(.white)
                .background(Color.accentColor)
                .cornerRadius(8)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .strokeBorder(Color.black, lineWidth: 3))
        }
    }
    
    func solvableCheck(){
        isReadyToSolve = eq.variables.filter(
            {
                $0.valueAsString != ""
            }).count >= 1
    }
}


#Preview {
    NavigationView{
        EquationDetail(eq: Equation.previewData[3])
    }.modelContainer(ModelData.previewContainer)
}


