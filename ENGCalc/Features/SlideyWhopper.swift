import SwiftUI
import SwiftData
import Foundation


struct SlideyWhopper: View {
    @State var presentSideMenu = true
    @State var currentFolder : String = "All"
    var body: some View {
        
        ZStack {
            Color.white.edgesIgnoringSafeArea(.all)
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .overlay(
                    ZStack {
                        HStack {
                            Button {
                                presentSideMenu.toggle()
                            } label: {
                                Image(systemName: "line.3.horizontal")
                                    .resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .foregroundColor(.black)
                            }
                            .frame(width: 24, height: 24)
                            .padding(.leading, 30)
                            Spacer()
                            Spacer()
                        }
                    }
                        .frame(maxWidth: .infinity)
                        .frame(height: 50)
                        .background(Color.white)
                        .zIndex(1)
                        .shadow(radius: 0.3)
                    , alignment: .top)
                .background(Color.black.opacity(0.8))
            SideMenu()
        }
        .frame(maxWidth: .infinity)
    }
    
    
    @ViewBuilder
    func SideMenu() -> some View {
        SideView(isShowing: $presentSideMenu, direction: .leading) {
            SideMenuViewContents(presentSideMenu: $presentSideMenu, currentFolder: $currentFolder)
                .frame(width: 250)
        }
    }
}

struct SideView<RenderView: View>: View {
    @Binding var isShowing: Bool
    var direction: Edge
    @ViewBuilder  var content: RenderView
    
    var body: some View {
        ZStack(alignment: .leading) {
            if isShowing {
                Color.black
                    .opacity(0.6)
                    .ignoresSafeArea()
                    .onTapGesture {
                        isShowing = false
                    }
                content
                    .transition(.move(edge: direction))
                    .background(
                        Color.white
                    )
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
        .ignoresSafeArea()
        .animation(.easeInOut, value: isShowing)
    }
}

struct SideMenuViewContents: View {
    
    @Binding var presentSideMenu: Bool
    @Binding var currentFolder: String
    @State var showingAlert: Bool = false
    @State var newFolderName: String = ""
    @Environment(\.modelContext) var modelContext
    @Query(sort: \Folder.name) var folderList: [Folder]
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading, spacing: 0) {
                SideMenuHeader()
                VStack {
                    List(folderList, id: \.self){ folder in
                        Button (action:{
                            currentFolder = folder.name.lowercased()
                        },
                                label:
                                    {
                            Text(folder.name)
                                .fontWeight(currentFolder.lowercased() == folder.name.lowercased() ? .bold : .regular)
                                .foregroundStyle(Color.black)
                        })
                        .contextMenu {
                            Button(role: .cancel) {
                                currentFolder = folder.name.lowercased()
                            } label: {
                                Label(
                                    "Select Folder" ,
                                    systemImage: "arrow.forward"
                                )
                            }
                            if (folder.deletable){
                                Button(role:  .destructive) {
                                    modelContext.delete(folder)
                                } label: {
                                    Label(
                                        "Delete",
                                        systemImage: "trash"
                                    )
                                }
                            }
                        }
                    }
                    .listStyle(.sidebar)
                }
                SideMenuFooter()
                
            }
            .frame(maxWidth: .infinity)
            .background(.white)
        }
    }
    
    func SideMenuHeader() -> some View {
        VStack {
            HStack {
                Button(action: {
                    presentSideMenu.toggle()
                }, label: {
                    Image(systemName: "x.circle")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(.black)
                })
                .frame(width: 20, height: 20)
                Text("EngCalc").fontWeight(.bold)
                Spacer()
            }
        }
        .frame(maxWidth: .infinity)
        .padding(.leading, 25)
        .padding(.top, 70)
        .padding(.bottom, 30)
    }
    
    func SideMenuFooter() -> some View {
        VStack {
            HStack {
                Button(action: {
                    showingAlert = true
                    
                }, label: {
                    HStack{
                        Image(systemName: "plus")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .foregroundColor(.black)
                            .frame(width: 15, height: 15)
                        
                        Text("Add Folder")
                            .padding(.trailing)
                            .foregroundColor(.black)
                    }
                })
                .alert("New Folder", isPresented: $showingAlert) {
                    TextField("New Folder Name", text: $newFolderName)
                    Button("Add", role: .cancel) {
                        Folder.create(name: newFolderName, modelContext: modelContext)
                        newFolderName = ""
                    }
                    Button("Cancel", role: .destructive) {
                        newFolderName = ""
                    }
                }
                Spacer()
                
            }
        }
        .frame(maxWidth: .infinity)
        .padding(.leading, 25)
        .padding(.top, 20)
        .padding(.bottom, 35)
    }
}

struct BurgerClick: View{
    @Binding var presentSideMenu: Bool
    var body: some View{
        ZStack {
            HStack {
                Button {
                    presentSideMenu.toggle()
                } label: {
                    Image(systemName: "line.3.horizontal")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .foregroundColor(.black)
                        .frame(width: 24, height: 24)
                        .padding(.leading, 10)
                }
                Spacer()
            }
        }
    }
}

#Preview {
    SlideyWhopper().modelContainer(ModelData.previewContainer)
}
