import Foundation
import SwiftUI
import SwiftData
import LaTeXSwiftUI

struct EquationsList: View{
    
    @Environment(\.modelContext) private var modelContext
    @Query(sort: \Equation.name) var equationList: [Equation]
    @State var presentSideMenu: Bool = false
    @State private var isPresentingEquationForm: Bool = false
    @State private var newEQFormData: Equation.FormData = Equation.FormData()
    @State private var currentFolder: String = "All"
    @Query(filter: #Predicate<Folder> { folder in
        folder.deletable
    } ,sort: \Folder.name) var folderList: [Folder]
    @State private var searchTerm = ""
    
    var filteredEquations: [Equation] {
        guard !searchTerm.isEmpty else { return equationList }
        return equationList.filter { $0.name.localizedCaseInsensitiveContains(searchTerm) }
    }
    
    var body: some View {
        ZStack{
            NavigationStack{
                List{
                    ForEach(filteredEquations.filter {eqFilter(eq: $0)} ){ eq in
                        NavigationLink(destination: EquationDetail(eq: eq)) {
                            EquationsRow(eq: eq)
                                .contextMenu {
                                    if (currentFolder.lowercased() != "all" && currentFolder.lowercased() != "defaults" && currentFolder.lowercased() != "user made") {
                                        Button(role: .destructive) {
                                            if let index = eq.folderList.firstIndex(where: { $0.lowercased() == currentFolder.lowercased() }) {
                                                eq.folderList.remove(at: index)
                                            }
                                        } label: {
                                            Label(
                                                "Remove from Folder",
                                                systemImage: "xmark.circle.fill"
                                            )
                                        }
                                    }
                                    if eq.deletable {
                                        Button(role:  .destructive) {
                                            modelContext.delete(eq)
                                        } label: {
                                            Label(
                                                "Delete",
                                                systemImage: "trash"
                                            )
                                        }
                                    }
                                }
                        }
                    }
                }
                .searchable(text: $searchTerm, prompt: Text("Search Equations"))
                .navigationBarTitle("Equations", displayMode: .inline)
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        BurgerClick(presentSideMenu: $presentSideMenu)
                    }
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button("Add") {
                            isPresentingEquationForm.toggle()
                        }
                    }
                }
                .sheet(isPresented: $isPresentingEquationForm) {
                    NavigationStack {
                        EquationForm(formData: $newEQFormData)
                            .toolbar {
                                ToolbarItem(placement: .navigationBarLeading) {
                                    Button("Cancel") { isPresentingEquationForm.toggle()
                                        newEQFormData = Equation.FormData()
                                    }
                                }
                                ToolbarItem(placement: .navigationBarTrailing) {
                                    Button("Save") {
                                        Equation.create(from: newEQFormData, context: modelContext)
                                        newEQFormData = Equation.FormData()
                                        isPresentingEquationForm.toggle()                }
                                }
                            }
                            .navigationTitle("Add Equation")
                    }
                }
                
            }.zIndex(/*@START_MENU_TOKEN@*/1.0/*@END_MENU_TOKEN@*/)
                .background(Color.clear)
            SideMenu().zIndex(2.0)
        }
        .onAppear {
            if equationList.isEmpty {
                ModelData.startingData(modelContext: modelContext)
            }
        }
    }
    
    @ViewBuilder
    func SideMenu() -> some View {
        SideView(isShowing: $presentSideMenu, direction: .leading) {
            SideMenuViewContents(
                presentSideMenu: $presentSideMenu,
                currentFolder: $currentFolder
            )
            .frame(width: 250)
        }
    }
    
    func eqFilter(eq: Equation) -> Bool{
        eq.folderList.contains{
            $0.lowercased() == currentFolder.lowercased()
        }
    }
}

struct EquationsRow: View{
    
    var eq: Equation
    
    var body: some View {
        HStack {
            AsyncImage(url: eq.thumbnailUrl) { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 100, height: 100)
                    .cornerRadius(6)
            } placeholder: {
                if eq.thumbnailUrl != nil {
                    ProgressView()
                        .frame(width: 100, height: 100)
                } else {
                    Image(systemName: "function")
                        .frame(width: 50, height: 50)
                        .background(Color.red)
                        .cornerRadius(8)
                        .padding(.trailing)
                }
            }
            
            VStack(alignment: .leading){
                Text(eq.name)
                LaTeX(eq.latex)
            }
            Spacer()
            Image(systemName: eq.fav ? "star.fill" : "star")
                .foregroundColor(eq.fav ? .yellow : .black)
                .onTapGesture {
                    if (!eq.fav){
                        eq.folderList.append("favorites")
                    }
                    else {
                        eq.folderList.removeAll(where: {$0.lowercased() == "favorites"})
                    }
                }
                .padding(.trailing, 10)
        }
        .padding()
    }
}

#Preview {
    return EquationsList().modelContainer(ModelData.previewContainer)
}
