//
//  EquationForm.swift
//  ENGCalc
//
//  Created by Michael on 3/27/24.
//

import SwiftUI
import SwiftData
import Foundation


struct EquationForm: View {
    
    @Environment(\.modelContext) var modelContext
    @Query(filter: #Predicate<Folder> { folder in
        folder.deletable
    } ,sort: \Folder.name) var folderList: [Folder]
    
    @Binding var formData: Equation.FormData
    @State var showingAlert: Bool = false
    @State var currFolderIdx = 0
    @State var currFolder: String = ""
    
    var body: some View {
        Form {
            VStack(alignment: .leading) {
                Spacer()
                Text("Add to Folders").modifier(FormLabel())
                VStack(alignment: .leading) {
                    
                    VStack(alignment: .leading) {
                        Text("Current Folders:").modifier(FormLabel()).padding(.bottom, 2)
                        ForEach(formData.folderList.filter{ foldername in folderList.contains( where: {foldername == $0.name} )}, id: \.self) { folderString in
                            HStack{
                                Text(folderString).font(.caption2)
                                Button {
                                    withAnimation {
                                        removeFolder(folderString)
                                    }
                                } label: { Image(systemName: "x.circle").foregroundStyle(.red)
                                }
                                .buttonStyle(.plain)
                            }.padding(.vertical, 2)
                        }
                    }
                    HStack{
                        if !folderList.isEmpty{
                            Picker(selection: $currFolderIdx, label: EmptyView()) {
                                ForEach(0..<folderList.count, id: \.self) { index in
                                    Text(folderList[index].name).font(.caption2)
                                }
                            }.labelsHidden()
                                .pickerStyle(.menu)
                        }
                        else {
                            Text("No Created Folders").padding()
                        }
                        Spacer()
                        Button {
                            withAnimation {
                                if !formData.folderList.contains(folderList[currFolderIdx].name) {
                                    formData.folderList.append(folderList[currFolderIdx].name)
                                }
                            }
                            currFolder = ""
                        } label: { Image(systemName: "plus.circle.fill") }
                            .buttonStyle(.plain)
                            .padding(.horizontal)
                    }
                    .overlay(RoundedRectangle(cornerRadius: 10.0).strokeBorder(
                        Color.black, style: StrokeStyle(lineWidth: 1.0)))
                    .pickerStyle(.menu)
                    .padding(.top, 2)
                }.padding()
                
                if (formData.deletable){
                    Section{
                        Spacer()
                        TextFieldWithLabel(label: "Equation Name", text: $formData.name, prompt: "")
                        TextFieldWithLabel(label: "Plain Equation", text: $formData.plainEquation, prompt: "")
                        TextFieldWithLabel(label: "Image URL", text: $formData.thumbnailUrl, prompt: "")
                        VariablesEditor(formData: $formData)
                        Spacer()
                        Spacer()
                        VStack(alignment: .leading) {
                            Text("Details").modifier(FormLabel())
                            TextEditor(text: $formData.details)
                                .frame(height: 130)
                        }
                        VStack(alignment: .leading) {
                            Text("Notes").modifier(FormLabel())
                            TextEditor(text: $formData.notes)
                                .frame(height: 130)
                        }
                    }
                    
                }
            }
        }
    }
    
    func removeFolder(_ folder: String) {
        if let index = formData.folderList.firstIndex(where: {$0 == folder}) {
            formData.folderList.remove(at: index)
        }
    }
}

struct VariablesEditor: View {
    @State var newVariableSymbol: String = ""
    @State var newVariableName: String = ""
    @State var newVariableUnit: String = ""
    var units = ["in", "cm", "km"]
    @Binding var formData: Equation.FormData
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Variables")
                .modifier(FormLabel())
            ForEach(formData.variables) { variable in
                HStack {
                    Text(variable.symbol)
                    
                    if (variable.name != ""){
                        Text(" - \(variable.name)")
                    }
                    if (variable.unit != ""){
                        Text("[\(variable.unit)]")
                    }
                    Button {
                        withAnimation {
                            removeVariable(variable)
                        }
                    } label: { Image(systemName: "x.circle").foregroundStyle(.red)
                    }
                    .buttonStyle(.plain)
                }
                .padding(.top, 3)
            }
            VStack(alignment: .leading) {
                Text("New Variable")
                    .font(.caption)
                    .bold()
                HStack {
                    TextField("Symbol", text: $newVariableSymbol)
                        .autocapitalization(.none)
                        .padding(3)
                        .background(RoundedRectangle(cornerRadius: 10).fill(.regularMaterial))
                        .onChange(of: newVariableSymbol) {
                            if newVariableSymbol.count > 1 {
                                newVariableSymbol = String(newVariableSymbol.prefix(1))
                            }
                        }
                    TextField("Name", text: $newVariableName)
                        .autocapitalization(.none)
                        .padding(3)
                        .background(RoundedRectangle(cornerRadius: 10).fill(.regularMaterial))
                    Spacer()
                    Button {
                        if (newVariableSymbol != ""){
                            let variable = Variable(name: newVariableName, symbol: newVariableSymbol, value: 0, unit: newVariableUnit)
                            if (!formData.variables.contains(where: {$0.name == variable.name })){
                                withAnimation {
                                    formData.variables.append(variable)
                                }
                            }
                            newVariableSymbol = ""
                            newVariableName = ""
                            newVariableUnit = ""
                        }
                    } label: { Image(systemName: "plus.circle.fill") }
                        .buttonStyle(.plain)
                }
            }
            .padding()
            .overlay(RoundedRectangle(cornerRadius: 10.0).strokeBorder(
                Color.black, style: StrokeStyle(lineWidth: 1.0)))
        }
    }
    
    func removeVariable(_ variable: Variable) {
        if let index = formData.variables.firstIndex(of: variable) {
            formData.variables.remove(at: index)
        }
    }
}

#Preview {
    @State var dataForForm = Equation.previewData[0].dataForForm
    return NavigationStack { EquationForm(formData: $dataForForm).modelContainer(ModelData.previewContainer)}
}

