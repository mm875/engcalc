import Foundation

struct NewtonAPIEndpoint {
    static let baseUrl = "https://newton.now.sh/api/v2"
    static let allowedCharacterSet = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._~")
    
    enum QueryType {
        case simplify
        case factor
        case derive
        case integrate
        case areaundercurve
        case cosine
        case sine
        case tangent
        case inversecosine
        case inversesine
        case inversetangent
        case logarithm
        var queryName: String{
            switch self{
            case .simplify: return "simplify"
            case .factor: return "factor"
            case .derive: return "derive"
            case .integrate: return "integrate"
            case .areaundercurve: return "areaundercurve"
            case .cosine: return "cosine"
            case .sine: return "sine"
            case .tangent: return "tangent"
            case .inversecosine: return "inversecosine"
            case .inversesine: return "inversesine"
            case .inversetangent: return "inversetangent"
            case .logarithm: return "logarithm"
            }
        }
    }
    static func path(queryType: QueryType, queryInput: String) -> String{
        let url = "\(baseUrl)/\(queryType.queryName)/"
        let expression = queryInput.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet) ?? ""
        return "\(url)\(expression)"/*.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "Error"*/
    }
}
