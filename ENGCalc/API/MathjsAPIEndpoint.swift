struct MathjsAPIEndpoint {
    static let baseUrl = "https://api.mathjs.org/v4/"
    
    static func path(queryInput: String) -> String{
        let url = "\(baseUrl)/?expr="
        return "\(url)\(queryInput)".addingPercentEncoding(withAllowedCharacters:.urlQueryAllowed) ?? "Error"
    }
}
