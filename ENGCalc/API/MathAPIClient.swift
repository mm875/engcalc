import Foundation

protocol MathAPI {
    func solveForVariable(eq: Equation) async throws -> String
}

struct MathAPIClient: MathAPI, APIClient {
    let session: URLSession = .shared
    
    static let greek_alphabet = "ΑαΒβΓγΔδΕεΖζΗηΘθΙιΚκΛλΜμΝνΞξΟοΠπΡρΣσςΤτΥυΦφΧχΨψΩω"
    static let latin_alphabet = "AaBbGgDdEeZzHhJjIiKkLlMmNnXxOoPpRrSssTtUuFfQqYyWw"
    
    let greekToLatinMapping: [Character: Character] = [
        "Α": "A", "α": "a",
        "Β": "B", "β": "b",
        "Γ": "G", "γ": "g",
        "Δ": "D", "δ": "d",
        "Ε": "E", "ε": "e",
        "Ζ": "Z", "ζ": "z",
        "Η": "H", "η": "h",
        "Θ": "J", "θ": "j",
        "Ι": "I", "ι": "i",
        "Κ": "K", "κ": "k",
        "Λ": "L", "λ": "l",
        "Μ": "M", "μ": "m",
        "Ν": "N", "ν": "n",
        "Ξ": "X", "ξ": "x",
        "Ο": "O", "ο": "o",
        "Π": "P", "π": "p",
        "Ρ": "R", "ρ": "r",
        "Σ": "S", "σ": "s", "ς": "s",
        "Τ": "T", "τ": "t",
        "Υ": "U", "υ": "u",
        "Φ": "F", "φ": "f",
        "Χ": "Q", "χ": "q",
        "Ψ": "Y", "ψ": "y",
        "Ω": "W", "ω": "w"
    ]
    
    func solveForVariable(eq: Equation) async throws -> String {
        let expr: String = substituteVariables(eq: eq)
        let path = NewtonAPIEndpoint.path(queryType: .simplify, queryInput: expr)
        let response: NewtonResponse = try await performRequest(url: path)
        return response.result
    }
    
    func substituteVariables(eq:Equation) -> String {
        var eqIndex = eq.plainEquation.firstIndex(of: "=") ?? eq.plainEquation.index(eq.plainEquation.startIndex, offsetBy: 3)
        eqIndex = eq.plainEquation.index(after: eqIndex)
        let simplifiedEquation = eq.plainEquation[eqIndex...]
        let expressionArray = simplifiedEquation.map { char in
            if let variable = eq.variables.first(where: { $0.lowerCaseSym == String(char) }), let value = variable.value {
                return String("(\(value))")
            } else {
                if (!MathAPIClient.greek_alphabet.contains( String(char))){
                    return String(char)
                }
                else{
                    if let latinChar = greekToLatinMapping[char] {
                        return String(latinChar)
                    } else {
                        return String(char)
                    }
                }
            }
        }
        let expression = expressionArray.joined(separator: "")
        return expression
    }
}

struct MockMathAPIClient: MathAPI {
    
    func solveForVariable(eq: Equation) async throws -> String {
        return "1"
    }
    
    
}

